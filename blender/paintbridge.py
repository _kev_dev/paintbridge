import bpy, requests, json, array, os
from io import BytesIO

bl_info = {
    "name" : "Paintbridge",
    "blender": (3, 6, 0),
    "category": "Texture Painting"
}


standard_header = {
    "Content-Type" : 'application/json'
}

base_url = "http://localhost:5050/paintbridge/{}"
addon_keymaps = []


def paintbridge_sync():
    temp_dir = get_temp_dir()
    files = os.listdir(temp_dir)

    width, height = (0, 0)
    for file in files:
        if 'png' in file:
            loaded_texture = load_texture(os.path.join(temp_dir, file))
            width, height = loaded_texture.size

    uv_filepath = os.path.join(temp_dir, 'uv.svg')

    bpy.ops.uv.export_layout(filepath=uv_filepath, mode='SVG', size=(width, height))


def get_temp_dir():
    response_get = requests.get(base_url.format("tempfolder"), headers=standard_header)
    response_data = json.loads(response_get.content)

    return response_data["folder"]

def load_texture(filepath):
    image_name = os.path.basename(filepath)

    if image_name in bpy.data.images:
        bpy.data.images.remove(bpy.data.images[image_name])

    bpy.data.images.load(filepath=filepath)
    return bpy.data.images[image_name]


def register():
    print("started")
    bpy.utils.register_class(PaintbridgeSyncOperator)
    wm = bpy.context.window_manager
    kc = wm.keyconfigs.addon

    if kc:
        km = kc.keymaps.new(name='Window', space_type='EMPTY')
        kmi = km.keymap_items.new('wm.paintbridge_sync', 'R', 'PRESS', shift=True, ctrl=True)
        addon_keymaps.append((km, kmi))


def unregister():
    bpy.utils.unregister_class(PaintbridgeSyncOperator)
    wm = bpy.context.window_manager
    kc = wm.keyconfigs.addon

    if kc:
        for km, kmi in addon_keymaps:
            km.keymap_itemsprint("started").remove(kmi)
        addon_keymaps.clear()


class PaintbridgeSyncOperator(bpy.types.Operator):
    bl_idname = "wm.paintbridge_sync"
    bl_label = "PaintBridge Sync"
    
    def execute(self, context):
        paintbridge_sync()
        return {'FINISHED'}


if __name__ == "__main__":
    register()