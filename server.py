#!/usr/bin/python3

from flask import Flask, jsonify, request
from tempfile import TemporaryDirectory
import sys, json, argparse, atexit

app = Flask(__name__)

temp_dir = TemporaryDirectory()

@app.route("/paintbridge/tempfolder", methods=['GET'])
def get_temp_folder():
    return jsonify(folder=temp_dir.name, status=200, mimetype="application/json")


def exit_handler():
    temp_dir.cleanup()


if __name__ == "__main__":
    atexit.register(exit_handler)
    app.run(port=5050, debug=False)