from krita import *
from urllib.request import urlopen
import json, os

base_url = "http://localhost:5050/paintbridge/{}"

standard_headers = {
    "Content-Type" : 'application/json'
}

doc = Krita.instance().activeDocument()
root = doc.rootNode()

if doc is not None:
    paintbridge_layer = doc.nodeByName("paintbridge")
    if not paintbridge_layer:
        paintbridge_layer = doc.createGroupLayer("paintbridge")
        doc.rootNode().addChildNode(paintbridge_layer , None)
        doc.refreshProjection()

    paintbridge_layer.setLocked(False)
    
    with urlopen(base_url.format("tempfolder")) as response:
        body = response.read()
        
    folder = json.loads(body)["folder"]
    paintbridge_layer.setVisible(False)
    
    uv_layer = doc.nodeByName("uv")
    if uv_layer:
        uv_layer.remove()
        
    uv_layer = doc.createVectorLayer("uv")
    
    if os.path.exists(os.path.join(folder, "uv.svg")):
        with open(os.path.join(folder, "uv.svg")) as f:
            svg_data = f.read()
            uv_layer.addShapesFromSvg(svg_data)
    paintbridge_layer.addChildNode(uv_layer, None)
    doc.refreshProjection()
        
    
    doc.setBatchmode(True)
    export_options = InfoObject()
    export_options.setProperty("indexed", False)
    export_options.setProperty("interlaced", False)
    export_options.setProperty("saveSRGBProfile", False)
    export_options.setProperty("forceSRGB", True)
    export_options.setProperty("alpha", True)
    
    png_image = os.path.basename(doc.fileName()).split('.')[0]
    doc.exportImage(os.path.join(folder, png_image + ".png"), export_options)

    paintbridge_layer.setVisible(True)
    paintbridge_layer.setLocked(True)
