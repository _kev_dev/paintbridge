### Paintbridge, a quick and dirty way to sync up Krita and Blender

This repo comes in three parts: A Blender Addon, a Krita script, and a Flask app to keep everything in sync.

#### Flask App
This is a simple script that must be started first before either the Krita or Blender portions can be used. It creates a temp folder and opens up a web endpoint that Krita and Blender can both read from in order to sync up.

It will clean up the temp folder when the app shuts down.

##### Installation / Running
1. Install the latest version of Python
2. In the command line, `pip install flask`
3. Run python server.py

#### Blender Addon

##### Installation
Just drag and drop this into your addons folder and activate in Blender. 

##### How to use
The sync hotkey is CTRL+SHIFT+R

##### When Ran It will
1. Load in any images in the sync temp folder
2. Export the UV map for the currently selected mesh

Note: You still can reload an image using Alt-R if you want. Though if you are coming back to a project, you should resync via Paintbridge at first before you do that in order to make sure all the files can be found from the newly created temp folder.

#### Krita Script

##### Installation
1. Open Krita
2. Go to Tools-> Scripts -> Ten Scripts, 
3. Assign this the script in the Krita folder to any hotkey

##### How to use
Sync should run with the hotkey you set up in installation.

##### When Ran it will
1. Create a group layer called "paintbridge", if not already found.
2. Import the UV map exported from Blender (if present) as a Vector layer in the "paintbridge" group folder.
3. Export all visible layers as a PNG out to the temp folder (the paintbridge layer gets hidden before export)

#### Issues or Concerns
Please feel free to reach out to me at @_kev_dev@mastodon.social if you have any comments or concerns.